import React from 'react'
import SocialButton from "./SocialButton"
import SignUpForm from "./SignUpForm"
// import SignInPage from "./SignInPage"
import "../styles/MainLayout.scss"
import {  Link } from 'react-router-dom'

class SignUpPage extends React.Component {
  render() {
    return (
      <div className="MainLayout">
        <div className="MainLayout__left-component">
          <div className="MainLayout__left-component--todos">
            <h4>Todos</h4>
          </div>
          <div className="MainLayout__left-component--signin">
            <h2 >Welcome Back!</h2>
            <p>To keep conected with us please login with your personal info</p>
            <Link to="/SignInPage" exact>
              <button className="signin-btn" >SIGN IN</button>
            </Link>
          </div>
        </div>
        <div className="MainLayout__right-component">
          <h1>Created Account</h1>
          <SocialButton />
          <SignUpForm />
        </div>
      </div>
    )
  }
}

export default SignUpPage