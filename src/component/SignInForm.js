import React, { Component } from 'react'
import {  Route, withRouter } from 'react-router-dom'
import axios from 'axios'

const BaseURL = "https://sukatodo.herokuapp.com"

class SignInForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: "",
      password: "",
      isLogin: true
    }
  }

  login = async (e) => {
    e.preventDefault()
    const LOGIN_ENDPOINT = `${BaseURL}/api/v1/users/login`
console.log(this.state)
    try {

      let response = await axios.post(LOGIN_ENDPOINT, this.state)
        console.log(response.data.data.token)
      if (response.status === 200 && response.data.data.token) {
        let token = response.data.data.token

        localStorage.setItem('access_token', token);
        this.props.history.push("/TaskDaskboard")
      }
    } catch (err) {
      console.log(err)
    }
  }
    
  handleInput =(e) =>{
    const key = e.target.name
    this.setState({
      [key]: e.target.value
    })
  }
  submit() {

  }

  render() {
    console.log(this.props)
    return (
      <Route>
        < div >
        <p>or use your email account</p>
          <form onSubmit={this.login}>
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.handleInput}
              // onChange={(event) => { this.updateEmail(event.target.value) }}
              value={this.state.email} />
            <br />
            <input
              type="password"
              name="password"
              placeholder="Password"
              onChange={this.handleInput}
              // onChange={(event) => { this.updatePassword(event.target.value) }}
              value={this.state.password} />
            <br />
            {/* <Link to="/TaskDaskboard"> */}
              <button
                className="signup-btn"
                // onClick={() => { this.submit() }}
                >SIGN IN</button>
            {/* </Link> */}
          </form>
        </div >
      </Route >
    )
  }
}
export default withRouter(SignInForm)