import React, { Component } from 'react'
import FacebookIcon from '../assets/facebook.svg'
import GooglePlusIcon from '../assets/google-plus.svg'
import LinkenInIcon from '../assets/linkendin.svg'
import "../styles/SocialButton.scss"

export default class SocialButton extends Component {
  render() {
    return (
      <div className="social">
        <div className="social__component">
          <img src={FacebookIcon} className="social__component--item" alt={FacebookIcon} />
          <img src={GooglePlusIcon} className="social__component--item" alt={GooglePlusIcon} />
          <img src={LinkenInIcon} className="social__component--item" alt={LinkenInIcon} />
        </div>
      </div>
    )
  }
}
