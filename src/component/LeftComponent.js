import React, { Component } from "react";
import "../styles/DaskboardLayout.scss";

class LefComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: "Reree",
        picture:
          "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Pig-512.png"
      }
    };
  }
  render() {
    return (
      <div className="DaskboardLayout__leftcomponent">
        <img src={this.state.user.picture} alt="profile"></img>
        <p> {this.state.user.name}</p>
        <p>My Day</p>
        <p>Important</p>
        <p>Completed </p>
      </div>
    );
  }
}

export default LefComponent;
