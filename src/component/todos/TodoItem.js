import React, { Component } from "react";
import "./list.scss";

class TodoItem extends Component {
  render() {
    const { _id, description, name } = this.props.todo;
    return (
      <div className="lists">
        <input
          type="checkbox"
          onChange={this.props.markComplete.bind(this, _id)}
        />
        <div className="lists__item">{name}</div>
        <div className="lists__item">{description}</div>
        <button onClick={this.props.deleteTodo.bind(this, _id)} className="fas fa-trash">Delete</button>
      </div>
    );
  }
}

export default TodoItem;
