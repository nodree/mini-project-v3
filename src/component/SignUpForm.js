import React, { Component } from 'react'
import { Route, withRouter} from 'react-router-dom'
import axios from 'axios'

const BaseURL = "https://sukatodo.herokuapp.com"

class SignUpForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: "",
      email: "",
      password: "",
      password_confirmation: "",
    }
  }
  register = async (e) => {
    e.preventDefault()
    const REGISTER_ENDPOINT = `${BaseURL}/api/v1/users`
console.log(this.state)
    try {

      let response = await axios.post(REGISTER_ENDPOINT, this.state)
        console.log(response.data.data.token)
      if (response.status === 201 && response.data.data.token) {
        let token = response.data.data.token
        localStorage.setItem('access_token', token);
        this.props.history.push("/TaskDaskboard")
      }
    } catch (err) {
      console.log(err)
    }
  }
  
  handleInput =(e) =>{
    const key = e.target.name
    this.setState({
      [key]: e.target.value
    })
  }
  submit() {

  }
  render() {
    return (
      <Route>
        <div>
          <p>or use your email for registration</p>
          <form onSubmit={this.register}>
            <input
              type="text"
              name="name"
              placeholder="Name"
              onChange={this.handleInput}
              value={this.state.name} />
            <br />
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.handleInput}
              value={this.state.email} />
            <br />
            <input
              type="password"
              name="password"
              placeholder="Password"
              onChange={this.handleInput}
              value={this.state.password} />
            <br />
            <input
              type="password"
              name="password_confirmation"
              placeholder="Password Confirmation"
              onChange={this.handleInput}
              value={this.state.password_confirmation} />
            <br />
            {/* <Link to="/TaskDaskboard"> */}
              <button
                className="signup-btn"
                // onClick={() => { this.submit() }}
                >SIGN UP</button>
            {/* </Link> */}
          </form>
        </div >
      </Route >
    )
  }
}

export default withRouter(SignUpForm)